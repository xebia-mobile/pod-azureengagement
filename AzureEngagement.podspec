Pod::Spec.new do |s|
  s.name         = "AzureEngagement"
  s.version      = "3.2.1"
  s.summary      = "Azure Mobile Engagement is a software-as-a-service (SaaS) user-engagement platform that provides data-driven insights into app usage, real-time user segmentation, and enables contextually-aware push notifications and in-app messaging."

  s.homepage     = "https://azure.microsoft.com"
  s.license      = { :type => "Copyright", :text => "" }

  s.author       = s.name

  s.platform     = :ios, "6.0"

  s.source       = { :git => "https://bitbucket.org/xebia-mobile/pod-azureengagement.git", :tag => s.version }

  s.library = 'xml2'
  s.frameworks = 'CoreLocation', 'CFNetwork', 'CoreTelephony', 'AdSupport', 'SystemConfiguration'
  s.vendored_libraries = 'libengagement.a', 'EngagementReach/libreach.a'
  s.source_files = "**/*.{h,m}"
  s.resources = ["EngagementReach/res/*"]
  s.requires_arc = true
end